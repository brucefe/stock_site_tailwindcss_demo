import "./index.css";

new TradingView.MediumWidget({
  symbols: [
    ["Apple", "AAPL"],
    ["Google", "GOOGL"],
    ["Microsoft", "MSFT"],
  ],
  chartOnly: false,
  width: "100%",
  height: "100%",
  locale: "en",
  colorTheme: "dark",
  gridLineColor: "rgba(42, 46, 57, 0)",
  fontColor: "#787b86",
  isTransparent: true,
  autosize: true,
  showFloatingTooltip: true,
  showVolume: false,
  scalePosition: "no",
  scaleMode: "Normal",
  fontFamily: "Trebuchet MS, sans-serif",
  noTimeScale: false,
  chartType: "area",
  lineColor: "#2962ff",
  bottomColor: "rgba(41, 98, 255, 0)",
  topColor: "rgba(41, 98, 255, 0.3)",
  container_id: "tradingview_d1460",
});
